package pt.ipp.isep.labdsoft.Analises.domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pt.ipp.isep.labdsoft.Analises.dto.PedidoAnaliseDto;

import java.time.LocalDateTime;

@RunWith(SpringJUnit4ClassRunner.class)

public class PedidoAnaliseTest {

    @Test
    public void fromDto(){
        PedidoAnaliseDto dto = new PedidoAnaliseDto("user",
                1L,TipoAnalise.GENETICA.toString()
                );
        PedidoAnalise pedido = PedidoAnalise.fromDTO(dto);
        Assertions.assertEquals(dto.idUtente,pedido.getIdUtente());
        Assertions.assertEquals(dto.tipoAnalise,pedido.getTipoAnalise().toString());
        Assertions.assertEquals(dto.usernameMedico,pedido.getUsernameMedico());


    }


    @Test
    public void toDto(){
        PedidoAnalise pedido = new PedidoAnalise("user",12L,TipoAnalise.VIRICA);
        PedidoAnaliseDto dto = pedido.toDTO();
        Assertions.assertEquals(dto.idUtente,pedido.getIdUtente());
        Assertions.assertEquals(dto.tipoAnalise,pedido.getTipoAnalise().toString());
        Assertions.assertEquals(dto.usernameMedico,pedido.getUsernameMedico());

    }


}
