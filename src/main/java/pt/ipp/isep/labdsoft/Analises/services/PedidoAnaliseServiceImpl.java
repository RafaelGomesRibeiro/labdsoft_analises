package pt.ipp.isep.labdsoft.Analises.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.ipp.isep.labdsoft.Analises.domain.PedidoAnalise;
import pt.ipp.isep.labdsoft.Analises.dto.PedidoAnaliseDto;
import pt.ipp.isep.labdsoft.Analises.repository.PedidoAnaliseRepository;

import java.util.List;
import java.util.NoSuchElementException;

@Service
public class PedidoAnaliseServiceImpl implements PedidoAnalisesService{

    @Autowired
    private PedidoAnaliseRepository pedidoAnaliseRepo;
    @Autowired
    private ValidaAtribuicaoService validaService;

    @Override
    public PedidoAnalise createPedido(PedidoAnaliseDto pdto) {
        if(!validaService.validaAtribuicaoServie(pdto)) throw new IllegalArgumentException("Utente não Existente");
        else return pedidoAnaliseRepo.save(PedidoAnalise.fromDTO(pdto));
    }

    @Override
    public List<PedidoAnalise> getPedidosByMedico(String usernameMedico) {
        return pedidoAnaliseRepo.findAllByUsernameMedico(usernameMedico).orElseThrow(()-> new NoSuchElementException("Pedidos nao encontrados"));
    }

    @Override
    public List<PedidoAnalise> getPedidosByUtente(Long id) {
        return pedidoAnaliseRepo.findAllByIdUtente(id).orElseThrow(()-> new NoSuchElementException("Pedidos nao encontrados"));
    }
}
