package pt.ipp.isep.labdsoft.Analises.domain;

import lombok.*;
import pt.ipp.isep.labdsoft.Analises.dto.PedidoAnaliseDto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
// @Table(name = "`candidaturas`", uniqueConstraints = {@UniqueConstraint(columnNames = {"cartaoCidadao"})})
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PedidoAnalise {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String usernameMedico;
    private Long idUtente;
    private LocalDateTime data;
    private TipoAnalise tipoAnalise;
    //TODO VERIFICAR SE ESTA SATISFEITO?

    public PedidoAnalise(String usernameMedico, Long idUtente, TipoAnalise tipoAnalise) {
        this.usernameMedico = usernameMedico;
        this.idUtente = idUtente;
        this.data = LocalDateTime.now();
        this.tipoAnalise = tipoAnalise;
    }

    public static PedidoAnalise fromDTO(PedidoAnaliseDto pedidoAnaliseDto){
        return new PedidoAnalise(pedidoAnaliseDto.usernameMedico, pedidoAnaliseDto.idUtente,TipoAnalise.valueOf(pedidoAnaliseDto.tipoAnalise));
    }

    public PedidoAnaliseDto toDTO(){
        return new PedidoAnaliseDto(this.usernameMedico, this.idUtente, this.tipoAnalise.toString());
    }
}
