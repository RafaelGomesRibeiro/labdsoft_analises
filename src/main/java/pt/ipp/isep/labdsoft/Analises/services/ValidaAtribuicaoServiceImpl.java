package pt.ipp.isep.labdsoft.Analises.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import pt.ipp.isep.labdsoft.Analises.dto.PedidoAnaliseDto;

@Service
public class ValidaAtribuicaoServiceImpl implements ValidaAtribuicaoService {

    private final WebClient webClient;
    @Value("${UTENTE_BASE_URL}"+"/api/utente/exists/")
    private String urlValidacaoUtente;

    public ValidaAtribuicaoServiceImpl(){
        this.webClient = WebClient.create();
    }

    @Override
    public Boolean validaAtribuicaoServie(PedidoAnaliseDto dto) {
        return webClient.get().uri(urlValidacaoUtente+dto.idUtente).retrieve().bodyToMono(Boolean.class).block();
    }
}
