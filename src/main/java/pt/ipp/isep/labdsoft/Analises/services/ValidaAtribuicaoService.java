package pt.ipp.isep.labdsoft.Analises.services;

import pt.ipp.isep.labdsoft.Analises.dto.PedidoAnaliseDto;

public interface ValidaAtribuicaoService {
    public Boolean validaAtribuicaoServie(PedidoAnaliseDto dto);
}
