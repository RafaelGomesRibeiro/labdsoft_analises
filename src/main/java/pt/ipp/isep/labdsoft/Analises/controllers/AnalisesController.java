package pt.ipp.isep.labdsoft.Analises.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.ipp.isep.labdsoft.Analises.domain.PedidoAnalise;
import pt.ipp.isep.labdsoft.Analises.dto.PedidoAnaliseDto;
import pt.ipp.isep.labdsoft.Analises.services.PedidoAnalisesService;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@RestController
@RequestMapping("analises")
public final class AnalisesController {

    @Autowired
    private PedidoAnalisesService pedidoAnalisesService;

     @GetMapping("/teste")
    public ResponseEntity<String> teste(){
         return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.TEXT_PLAIN).body("ola");
     }

     @PostMapping("/")
    public ResponseEntity<PedidoAnaliseDto> create(@RequestBody PedidoAnaliseDto pdto){
         return ResponseEntity.status(HttpStatus.OK).body(pedidoAnalisesService.createPedido(pdto).toDTO());
     }

     @GetMapping("/medico/{id}")
     public ResponseEntity<List<PedidoAnaliseDto>> getByMedico(@PathVariable String usernameMedico){
         return ResponseEntity.status(HttpStatus.OK).body(pedidoAnalisesService.getPedidosByMedico(usernameMedico).stream().map(PedidoAnalise::toDTO).collect(Collectors.toList()));
     }

     @GetMapping("/utente/{id}")
     public ResponseEntity<List<PedidoAnaliseDto>> getByUtente(@PathVariable Long id){
         return ResponseEntity.status(HttpStatus.OK).body(pedidoAnalisesService.getPedidosByUtente(id).stream().map(PedidoAnalise::toDTO).collect(Collectors.toList()));
     }

}
