package pt.ipp.isep.labdsoft.Analises.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.ipp.isep.labdsoft.Analises.domain.PedidoAnalise;

import java.util.List;
import java.util.Optional;

public interface PedidoAnaliseRepository extends JpaRepository<PedidoAnalise, Long> {

    public Optional<List<PedidoAnalise>> findAllByUsernameMedico(String usernameMedico);
    public Optional<List<PedidoAnalise>> findAllByIdUtente(Long idUtente);
}
