package pt.ipp.isep.labdsoft.Analises.services;

import pt.ipp.isep.labdsoft.Analises.domain.PedidoAnalise;
import pt.ipp.isep.labdsoft.Analises.dto.PedidoAnaliseDto;

import java.util.List;

public interface PedidoAnalisesService {

    public PedidoAnalise createPedido(PedidoAnaliseDto pdto);
    public List<PedidoAnalise> getPedidosByMedico(String usernameMedico);
    public  List<PedidoAnalise> getPedidosByUtente(Long id);
}
