package pt.ipp.isep.labdsoft.Analises.dto;

import lombok.*;
import pt.ipp.isep.labdsoft.Analises.domain.TipoAnalise;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PedidoAnaliseDto {

    public String usernameMedico;
    public Long idUtente;
    public String tipoAnalise;


}
