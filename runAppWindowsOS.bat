
@ECHO OFF
call gradlew build -x check
call docker network ls|grep labdsoft_network > nul || docker network create --driver bridge labdsoft_network
call docker container inspect rabbitMQ > nul || docker-compose up -d rabbit
call docker container inspect databases > nul || docker-compose up -d postgres
for /f %%i in ('docker ps -qf "name=^rabbitMQ"') do set containerId=%%i
echo %containerId%
If "%containerId%" == "" (
  echo "RabbitMQ not running, starting it up..."
  call docker start rabbitMQ
) ELSE (
  echo "Rabbit already running"
)
for /f %%i in ('docker ps -qf "name=^databases"') do set containerId1=%%i
echo %containerId1%
If "%containerId1%" == "" (
  echo "Databases not running, starting it up..."
  call docker start databases
) ELSE (
  echo "Databases already running"
)
call docker-compose up -d --build web
SLEEP 10
start "" http://localhost:9105/api/documentation
@PAUSE